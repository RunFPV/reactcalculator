# ReactCalculator

A beautiful calculator only built with a mac computer, a brain and time. The goal is only to increase my React Skills... And maybe to give me the opportunity to be hired in a compagny who works with React Technologies.

With this WebApp, users have to register and login to use the calculator. They also can update their informations.
When a user will register, his informations will be stored in localstorage (in fakeDB). Password is hashed and a unique token is created.
When a user will connect, his token is set in localstorage (in token item) and a global state is set. Token can easy identify user (for exemple edit his informations)
If the WebApp is refreshed, global state will be lost and token will be deleted (that's a choice).

Calculator is strongly inspired by the iPhone calculator app because.... I'm an Apple addict. It can make simple calculations like add, remove, multiply and divide. It also can make the opposite and apply percentage . All of this with int and float numbers.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installing

First step, clone the entire project and go in reactcalculator_front folder

```
git clone git@gitlab.com:pastequefrite/reactcalculator.git
cd reactcalculator/reactcalculator_front
```

Then, install packages

```
yarn install
```

## Running

Now you are able to run project 

```
yarn run
```

## Authors

* **Grégoire Amann** - *Initial work* - [CodeWhile](https://gitlab.com/pastequefrite)

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/pastequefrite/reactcalculator/blob/master/LICENSE) file for details
