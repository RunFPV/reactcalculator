import bcrypt from 'bcryptjs'
import UIDGenerator from 'uid-generator';

const uidgen = new UIDGenerator();


// Parse fakeDB to get data, if no data then create empty array.
const getFakeDB = () => {
    // Parse fakeDB to get data
    let fakeDB = JSON.parse(localStorage.getItem('fakeDB'));
    // If currentDB is Null, define it as an array
    if (!fakeDB) {
        fakeDB = [];
    }
    return fakeDB;
};


// Get userInfos with unique token
export const getUserInfos = (token) => {
    let userInfos= {};
    let currentDB = getFakeDB();

    currentDB.forEach(item => {
       if (item.token === token) {
           userInfos= item;
       }
    });

    delete userInfos.password;
    return userInfos;
};

const backEndValidation = (data, fakeDB) => {
    // Check if one of data props is undefined or null
    for (let key in data) {
        if (!data[key]) {
            throw new Error(key + " is required")
        }
    }

    // Check if password and confirm password do match
    if (data.password !== data.password2) {
        throw new Error("Password and confirm password do not match");
    }

    // Check if Username and/or email already taken only by another (requested token)
    fakeDB.forEach(item => {
        if (item.username === data.username && item.token !== data.token) {
            throw new Error("Username already taken");
        }
        if (item.email === data.email && item.token !== data.token) {
            throw new Error("Email already taken");
        }
    });

    // Remove Password2
    delete data.password2;
};


export const login = (credentials) => {
    let connected = false;

    // Getting Data from FakeDB
    let currentDB = getFakeDB();

    // Search for Username
    currentDB.forEach(item => {

        // If user and password found for the same item, store token in local storage and return true
        if (item.username === credentials.username && bcrypt.compareSync(credentials.password, item.password)) {
            localStorage.setItem('token', item.token);
            connected = true;
        }
    });

    return connected;
};


export const register = async (data) => {
    let registered = false;

    // Getting Data from FakeDB
    let currentDB = getFakeDB();

    // Check if username or email already taken, validation and remove password2
    backEndValidation(data, currentDB);

    // Hash Pass && create unique token
    bcrypt.hash(data.password, 10, (err, hash) => {
        if (err) {
            throw new Error(err);
        }
        data.password = hash;

        uidgen.generate((err, uid) => {
            if (err) {
                throw new Error(err);
            }
            data.token = uid;

            // Add new registration
            currentDB.push(data);
            localStorage.setItem('fakeDB', JSON.stringify(currentDB));
            registered = true;
        });
    });

    return registered;
};


// Edit userInfos with unique token
export const editUserInfos = async (data) => {
    // Getting Data from FakeDB
    let currentDB = getFakeDB();

    // Check if username or email already taken, validation and remove password2
    backEndValidation(data, currentDB);

    // Hash Pass && save Changes
    bcrypt.hash(data.password, 10, (err, hash) => {
        if (err) {
            throw new Error(err);
        }
        data.password = hash;

        let foundIndex = currentDB.findIndex(x => x.token === data.token);

        // If item found
        if (foundIndex !== -1) {
            currentDB[foundIndex] = data;
            localStorage.setItem('fakeDB', JSON.stringify(currentDB));
        }
    });
};
