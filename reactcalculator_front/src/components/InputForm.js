import React from 'react';

import {Form, InputGroup, FormControl} from 'react-bootstrap';
import PropTypes from 'prop-types';

import {css, StyleSheet} from 'aphrodite';


function InputForm(props) {
    return (
        <Form.Group>
            <Form.Control.Feedback type="invalid" className={css(styles.feedBack)}>{props.feedBackText}</Form.Control.Feedback>

            <div className={css(styles.customLbl)}>
                <Form.Label>{props.label ? props.label : props.name}</Form.Label>
                {props.labelHelp}
            </div>

            {props.inputGroup ?
                <InputGroup>
                    <FormControl onChange={props.onChange} onBlur={props.onBlur} onFocus={props.onFocus} required={props.required} type={props.isPassword ? "password" : "text"} name={props.name}
                                 placeholder={props.placeholder ? props.placeholder : props.name}
                                 ref={props.register}/>
                    <InputGroup.Append>
                        {props.inputGroup}
                    </InputGroup.Append>
                </InputGroup>
                :
                <Form.Control onBlur={props.onBlur} onFocus={props.onFocus} required={props.required} type={props.isPassword ? "password" : "text"} name={props.name} placeholder={props.placeholder ? props.placeholder : props.name}
                              ref={props.register}/>
            }
        </Form.Group>
    )
}

InputForm.propTypes = {
    feedBackText: PropTypes.string,
    name: PropTypes.string.isRequired,
    register: PropTypes.func.isRequired,
    required: PropTypes.bool,
    isPassword: PropTypes.bool,
    labelHelp: PropTypes.object,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    inputGroup: PropTypes.object,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
};

const styles = StyleSheet.create({
    customLbl: {
        display: 'flex',
    },
    feedBack: {
        display: 'flex',
    }
});

export default InputForm
