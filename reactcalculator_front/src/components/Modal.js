import React from 'react';

import {NavLink} from 'react-router-dom';

import PropTypes from 'prop-types';

import {Button, Modal as BtModal} from 'react-bootstrap';

import {css, StyleSheet} from 'aphrodite';
import theme from "../theme/theme";

class Modal extends React.Component {
    render() {
        return (
            <div>
                <BtModal show={this.props.open} onHide={this.props.toggle}>
                    <BtModal.Header closeButton className={css(styles.center)}><span role="img" aria-label="hello">👋</span> Registered successfully !</BtModal.Header>
                    <BtModal.Body>
                        <p className={css(styles.text)}>
                            Welcome to my ReactCalculator WebApp, I'm glad you're here ! We are already... one to use this awesome WebApp everyday.
                            Maybe some bugs are still here. Please feel free to contact me if you discover one of them!
                        </p>
                    </BtModal.Body>
                    <BtModal.Footer className={css(styles.center)}>
                        <Button to="/login" as={NavLink} className={css(styles.button)}>Log in</Button>
                    </BtModal.Footer>
                </BtModal>
            </div>
        );
    }
}

Modal.propTypes = {
    open: PropTypes.bool.isRequired,
    toggle: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
    center: {
        justifyContent: 'center',
    },
    text: {
        textAlign: 'justify',
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'center',
    },
    button: {
        backgroundColor: theme.PRIMARY_COLOR,
        borderColor: theme.PRIMARY_COLOR,
        // justifyContent: 'center',
    },
});

export default Modal;
