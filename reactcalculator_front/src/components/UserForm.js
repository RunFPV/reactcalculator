import React, {useEffect, useState} from 'react';

import {NavLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import useForm from 'react-hook-form';
import {Form, Button} from 'react-bootstrap';

import HelpButton from '../components/HelpButton';
import InputForm from "./InputForm";

import {getUserInfos} from '../back/fakeApi';

import {css, StyleSheet} from 'aphrodite';
import theme from '../theme/theme';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEye, faEyeSlash} from '@fortawesome/free-solid-svg-icons'


function UserForm(props) {
    const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;

    const {register, handleSubmit, errors, watch, setValue} = useForm();

    const [showPass, setShowPass] = useState(false);
    const [show, setShow] = useState(false);

    // Called like componentDidMount
    useEffect(() => {
        _settingValue(localStorage.getItem('token'));

        // YES, I cheated but idk why i've got this warning....
        // eslint-disable-next-line
    }, []);

    // Set user values only if not on registration
    const _settingValue = () => {
        const userInfos = getUserInfos(localStorage.getItem('token'));
        if (!props.isRegistration) {
            for (let key in watch()) {
                setValue(key, userInfos[key]);
            }
        }
    };

    // Display HaveAccount only on registration
    const _displayHaveAccount = () => {
        if (props.isRegistration) {
            return (
                <div className={css(styles.linkContainer)}>
                    <NavLink to="/login">Already have an account ?</NavLink>
                </div>
            )
        }
    };

    const checkErrors = (errors) => {
        if (errors) {
            switch (errors.type) {
                case 'required':
                    return "This field is required";
                case 'maxLength':
                    return "This field is too long";
                case 'minLength':
                    return "This field is too short";
                case 'pattern':
                    return "This field is invalid";
                case 'validate':
                    return "Password and confirm password do not match";
                default:
                    break;
            }
        }
    };

    return (
        <div>
            <Form className={css(styles.form)}>
                <Form.Control.Feedback type="invalid"
                                       className={css(styles.feedBack)}>{props.globalError}
                </Form.Control.Feedback>
                <Form.Control.Feedback type="valid"
                                       className={css(styles.feedBack)}>{props.globalInfo}
                </Form.Control.Feedback>

                {/* USERNAME INPUT */}
                <InputForm required
                           name="username"
                           label="Username"
                           feedBackText={checkErrors(errors.username)}
                           register={register({required: true, maxLength: 25, minLength: 5})}
                />

                {/* EMAIL INPUT */}
                <InputForm required
                           name="email"
                           label="Email"
                           feedBackText={checkErrors(errors.email)}
                           register={register({required: true, pattern: emailRegex})}
                />

                {/* PASSWORD INPUT */}
                <InputForm required
                           onChange={(e) => {e.target.value.length > 3 ? setShow(false) : setShow(true)}}
                           onFocus={() => setShow(true)}
                           onBlur={() => setShow(false)}
                           labelHelp={
                               <HelpButton
                                   show={show}
                                   onClick={() => setShow(!show)}
                                   placement={"top"}
                                   body={"Password must contain minimum 8 characters, at least 1 uppercase letter, 1 lowercase letter and 1 number. Special chars are not allowed"}
                               />
                           }
                           name="password"
                           label="Password"
                           isPassword={!showPass}
                           feedBackText={checkErrors(errors.password)}
                           register={register({required: true, pattern: passwordRegex})}
                           inputGroup={
                               <Button onClick={() => setShowPass(!showPass)}
                                       tabIndex="-1"
                                       className={css(styles.button)}><FontAwesomeIcon
                                   icon={showPass ? faEyeSlash : faEye}/></Button>
                           }
                />

                {/* CONFIRM PASSWORD INPUT */}
                <InputForm required name="password2" placeholder="Confirm password" label="Confirm password"
                           isPassword={!showPass}
                           feedBackText={checkErrors(errors.password2)}
                           register={register({
                               required: true, validate: (value) => {
                                   return value === watch('password'); // value is from password2 and watch will return value from password1
                               }
                           })}
                />

                {/* SUBMIT BUTTON */}
                <div className={css(styles.buttonContainer)}>
                    <Button type="submit"
                            className={css(styles.button)}
                            onClick={handleSubmit(props.onSubmit)}>{props.isRegistration ? "Register" : "Edit"}
                    </Button>
                </div>

                {_displayHaveAccount()}
            </Form>
        </div>
    )
}

UserForm.propTypes = {
    isRegistration: PropTypes.bool,
    onSubmit: PropTypes.func.isRequired,
    globalError: PropTypes.string,
    globalInfo: PropTypes.string,
};

const styles = StyleSheet.create({
    buttonContainer: {
        display: 'flex',
        justifyContent: 'center',
    },
    button: {
        backgroundColor: theme.PRIMARY_COLOR,
        borderColor: theme.PRIMARY_COLOR,
    },
    linkContainer: {
        textAlign: 'center',
    },
    form: {
        maxWidth: 500,
        margin: 'auto',
    },
    feedBack: {
        display: 'flex',
    }
});

export default UserForm
