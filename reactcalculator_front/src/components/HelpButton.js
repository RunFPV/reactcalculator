import React, {useRef} from 'react';
import {Button, Overlay, Tooltip} from 'react-bootstrap';

import {faQuestionCircle} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {css, StyleSheet} from 'aphrodite';

import PropTypes from 'prop-types';


function HelpButton(props) {
    const target = useRef(null);

    return (
        <div className={css(styles.container)}>
            <Button tabIndex="-1" ref={target} className={css(styles.button)} onClick={props.onClick}>
                <FontAwesomeIcon icon={faQuestionCircle}/>
            </Button>
            <Overlay target={target.current} show={props.show} placement={props.placement}>
                <Tooltip>{props.body}</Tooltip>
            </Overlay>
        </div>
    )
}

HelpButton.propTypes = {
    placement: PropTypes.string,
    body: PropTypes.string.isRequired,
    show: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    container: {
        paddingLeft: 5,
    },
    button: {
        background: 'none',
        color: 'black',
        border: 'none',
        padding: 0,
    }
});


export default HelpButton;
