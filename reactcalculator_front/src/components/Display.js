import React from 'react';

import PropTypes from 'prop-types';
import {css, StyleSheet} from 'aphrodite';

class Display extends React.Component {

    componentDidUpdate() {
        this.messagesEnd.scrollIntoView();
    }

    render() {
        return (
            <div className={css(styles.container)}>
                {
                    this.props.operations.map((op) => {
                        return <div key={op.key} className={css([styles.previousContent, styles.bot])}>{op.res}</div>
                    })
                }
                <div ref={(el) => {
                    this.messagesEnd = el;
                }} className={css([styles.content, styles.bot])}>{this.props.content}</div>
            </div>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column',
        // alignItems: 'flex-end',
        overflow: 'scroll',
        height: 300, // <-- safari
        width: '100%',
    },
    previousContent: {
        color: 'white',
        fontSize: '3em',
    },
    bot: {
        marginTop: 'auto',
    },
    content: {
        color: 'white',
        fontSize: '5em',
    }
});

Display.propTypes = {
    operations: PropTypes.array,
    content: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
};

export default Display
