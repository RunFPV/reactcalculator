import React from 'react';
import PropTypes from 'prop-types';


import {css, StyleSheet} from 'aphrodite';


class Button extends React.Component {
    constructor(props) {
        super(props);

        if (this.props.sustain) {
            this.buttonOperation = styles.sustainButton
        }

        if (this.props.zeroBtn) {
            this.zeroButton = styles.zeroButton
        }

        switch (this.props.color) {
            case "black":
                this.buttonColor = styles.buttonBlack;
                break;
            case "grey":
                this.buttonColor = styles.buttonGrey;
                break;
            default:
                this.buttonColor = styles.buttonOrange;
                break;
        }
    }


    _click = () => {
        this.props.onClick(this.props.name);
    };

    render() {
        return (
            <div className={css(styles.container, this.zeroButton)}>
                <button className={css(styles.button, this.buttonColor, this.buttonOperation)}
                        onClick={this._click}>{this.props.name}</button>
            </div>
        )
    }
}

Button.propTypes = {
    name: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    color: PropTypes.oneOf(['orange', 'grey', 'black']),
    sustain: PropTypes.bool,
    zeroBtn: PropTypes.bool,
};

const styles = StyleSheet.create({
    container: {
        padding: 5,
        width: 80,
        height: 80,
    },
    button: {
        color: 'white',
        fontSize: '2em',
        borderRadius: '50%',
        alignContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        textAlign: 'center',
        border: 'none',
        ':active': {
            opacity: '0.5',
        }
    },
    buttonOrange: {
        backgroundColor: '#fdcb6e',
    },
    buttonGrey: {
        backgroundColor: '#b2bec3',
        color: 'black',
    },
    buttonBlack: {
        backgroundColor: '#2d3436',
    },
    sustainButton: {
        ':focus': {
            color: '#fdcb6e',
            backgroundColor: 'white',
        }
    },
    zeroButton: {
        width: 175,
    },
});

export default Button
