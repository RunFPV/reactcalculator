import React from 'react';

import {css, StyleSheet} from 'aphrodite';

import Display from './Display';
import ButtonsPanel from './ButtonsPanel';

import {calcul} from '../back/calcul';


class Calculator extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            total: 0,
            prev: null,
            operator: null,
            operations : [],
            opId: 0,
        }
    }

    _onClick = (buttonName) => {
        this.setState(calcul(this.state, buttonName))
    };

    render() {
        return(
            <div className={css(styles.container)}>
                <Display operations={this.state.operations} content={this.state.total}/>
                <ButtonsPanel onClick={this._onClick}/>
            </div>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: '350px',
        height: '750px',
        backgroundColor: 'black',
        borderRadius: 50,
        // boxShadow: '1px 1px 100px black',
        padding: 15,
        display: 'flex',
        flexDirection: 'column',
        alignContent: 'center',
        justifyContent: 'space-around',
    },
});

export default Calculator
