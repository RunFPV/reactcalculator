import React from 'react';

import HomePage from '../pages/HomePage';
import UserPage from '../pages/UserPage';
import LoginPage from '../pages/LoginPage';
import RegisterPage from '../pages/RegisterPage';
import LogoutPage from '../pages/LogoutPage';


export default [
    {
        key: 1,
        path: '/',
        name: 'Home',
        Component: <HomePage/>,
        allowAnonymous : false,
        exact: true,
    },
    {
        key: 2,
        path: '/account',
        name: 'User',
        Component: <UserPage/>,
        allowAnonymous : false,
        exact: false,
    },
    {
        key: 3,
        path: '/login',
        name: 'Sign in',
        Component: LoginPage,
        allowAnonymous : true,
        exact: false,
    },
    {
        key: 4,
        path: '/register',
        name: 'Register',
        Component: RegisterPage,
        allowAnonymous : true,
        exact: false,
    },
    {
        key: 5,
        path: '/logout',
        name: 'Logout',
        Component: <LogoutPage/>,
        allowAnonymous : false,
        exact: false,
    }
];
