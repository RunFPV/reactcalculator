export default {
    PRIMARY_COLOR: '#219db6',
    SECONDARY_COLOR: '#7b7d7e',
    LIGHT_COLOR: 'white',
}
