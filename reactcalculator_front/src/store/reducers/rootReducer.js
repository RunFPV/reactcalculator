import {LOGIN, LOGOUT} from '../constants/action-types';

const initialState = {
    isLogged: false
};


function rootReducer(state = initialState, action) {
    let nextState;
    switch (action.type) {
        case LOGIN:
            nextState = {
                ...state,
                isLogged: true,
            };
            return nextState || state;
        case LOGOUT:
            nextState = {
                ...state,
                isLogged: false,
            };
            return nextState || state;
        default:
            return state;
    }
}

export default rootReducer;
