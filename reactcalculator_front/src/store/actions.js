import {LOGIN, LOGOUT} from './constants/action-types';

export function login() {
    return {type: LOGIN}
}

export function logout() {
    return {type: LOGOUT}
}
